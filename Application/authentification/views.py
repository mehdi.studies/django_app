from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from .models import AuthenticationUsers, Plan, UserOperation
from .form import SignupForm  # Renamed form.py to forms.py
import deepl
from .utils import *


def index(request):
    return render(request, 'index.html')


# View for user registration
def signup(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.errors:
            print(form.errors)
        if form.is_valid():
            # Save the user's registration data from the form
            form.save()

            # Determine user's plan and set allowed login attempts
            plan = request.POST['plan']
            plan = Plan.objects.get(plan_name=plan)

            # Create a new user instance and save it
            new_user = AuthenticationUsers(
                username=request.POST['username'],
                first_name=request.POST['first_name'],
                last_name=request.POST['last_name'],
                email=request.POST['email'],
                plan=plan,
            )
            new_user.save()

            return redirect('login')  # Redirect to the login page
    else:
        form = SignupForm()

    return render(request, 'signup.html', {'form': form})


# View for user login
def user_login(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)

        if user is not None:
            login(request, user)  # Authenticate and log in the user
            return redirect('next')  # Redirect to the next page after successful login
        else:
            error_message = "Invalid username or password. Please try again."
    else:
        error_message = ""

    return render(request, 'login.html', {'error_message': error_message})


# View for the next page (destination after successful login)
def next_page(request):
    current_user = request.user
    user = AuthenticationUsers.objects.get(username=current_user)
    user_operation = UserOperation.objects.get_or_create(user=user)[0]
    remain_words = user.plan.max_operation - user_operation.count_words
    context = {
        'total_operations': 0,
        'remaining_operations': remain_words
    }
    source_lang = request.POST.get('from-language')
    target_lang = request.POST.get('to-language')
    source_text = request.POST.get('translation-text')
    if source_text and target_lang and source_lang:
        if len(source_text.split(' ')) <= remain_words:
            translator = deepl.Translator("0f6e66d9-421e-213f-52c4-885fb6533824:fx")
            result = translator.translate_text(source_text, target_lang=target_lang)
            target_text = result.text
            remain_words -= len(source_text.split(' '))
            user_operation.count_words = user_operation.count_words + len(source_text.split(' '))
            user_operation.source_lang = source_lang
            user_operation.target_lang = target_lang
            user_operation.save()
            context['to_language'] = target_lang
            context['from_language'] = source_lang
            context['target_text'] = target_text
            context['translation_text'] = source_text
            context['total_operations'] = len(source_text.split(' '))
            context['remaining_operations'] = remain_words
        else:
            context['message'] = "Please upgrade your plan, no remaining words"
    return render(request, 'next.html', context)


def users_detail(request):
    users = AuthenticationUsers.objects.all()
    print(users)
    return render(request, 'clients.html', {'client': users})
