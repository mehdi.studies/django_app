from django.contrib import admin
from .models import AuthenticationUsers, Plan, UserOperation

admin.site.register(AuthenticationUsers)
admin.site.register(Plan)
admin.site.register(UserOperation)
# Register your models here.
