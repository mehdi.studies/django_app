from django.db import models
from datetime import datetime


# Create your models here.

class Plan(models.Model):
    plan_name = models.TextField(max_length=15)
    max_operation = models.IntegerField()

    class Meta:
        db_table = "plans"

    def __str__(self):
        return self.plan_name


class AuthenticationUsers(models.Model):
    username = models.TextField()
    first_name = models.TextField()
    last_name = models.TextField()
    email = models.TextField(blank=True, null=True)
    plan = models.ForeignKey(Plan, on_delete=models.DO_NOTHING)
    count_operation = models.IntegerField(default=0)

    class Meta:
        db_table = "authentication_users"

    def __str__(self):
        return self.username


class UserOperation(models.Model):
    user = models.ForeignKey(AuthenticationUsers, on_delete=models.DO_NOTHING)
    date_operation = models.DateTimeField(default=datetime.now)
    count_words = models.IntegerField(default=0)
    source_lang = models.TextField(max_length=20, default="EN")
    target_lang = models.TextField(max_length=10, default="EN")

    class Meta:
        db_table = "user_operations"


